const db = require("./db");
const express = require("express");
const router = express.Router();

router.post("/signin", (request, response) => {
  const { name, password } = request.body;
  const statement = `select id, Name,Role from Users where Name = '${name}' and password = '${password}'`;
  db.query(statement, (error, admins) => {
    if (error) {
      response.send({ status: "error", error: error });
    } else {
      if (admins.length == 0) {
        console.log("admin " + admins["Role"]);
        response.send({ status: "error", error: "admin does not exist" });
      } else {
        const admin = admins[0];
        if (admin["Role"] == "ADMIN") {
          console.log("Success!");
          //res.redirect("/UserHomePage");
          response.send({
            firstName: admin["Name"],
            msg: "Successfully login",
          });
        }
      }
    }
  });
});

// All members with their subscription
router.get("/members", (request, response) => {
  const statement = `
      select u.id, u.Name, u.Role,
        s.Subscriptionstartdate as Subscriptionstartdate,
        s.Subscriptionenddate as Subscriptionenddate,
        s.Subscriptionamount as Subscriptionamount
        from Users u
        inner join Membersubscription s 
        on u.id = s.userid
  `;

  db.query(statement, (error, data) => {
    if (error) {
      response.send(error);
    } else {
      response.send(data);
    }
  });
});

// add user
router.post("/add-user", (request, response) => {
  const { Name, Role, Salary, TotalExperience, Joindate } = request.body;
  const statement = `insert into Users (Name, Role,Salary,TotalExperience,Joindate) values ('${Name}', '${Role}','${Salary}', '${TotalExperience}','${Joindate}')`;
  db.query(statement, (error, data) => {
    response.send("User added successfully");
  });
});

// update user
router.put("/:id", (request, response) => {
  const { id } = request.params;
  const { Name, Role, Salary, TotalExperience } = request.body;
  const statement = `update Users set Name = '${title}', Role= '${title}',Salary= '${title}',TotalExperience= '${title}' where id = ${id}`;
  db.query(statement, (error, data) => {
    response.send("User edited successfully");
  });
});

// delete user
router.delete("/:id", (request, response) => {
  const { id } = request.params;
  const statement = `delete from Users where id = ${id}`;

  db.query(statement, (error, data) => {
    response.send("User deleted successfully");
  });
});

module.exports = router;
