const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const adminRouter = require("./admin");

//const mongoose = require("mongoose");
const app = express();

app.use(cors("*"));
app.use(bodyParser.json());

// db connection

// add the routes
app.use("/admin", adminRouter);

// default route
app.get("/", (request, response) => {
  response.send("welcome to my application");
});

app.listen(4000, "0.0.0.0", () => {
  console.log("server started on port 4000");
});
