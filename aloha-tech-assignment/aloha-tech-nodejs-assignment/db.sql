create database aloha-tech-db;

USE aloha-tech-db;

create table user (
	id integer PRIMARY KEY auto_increment, 
	Name VARCHAR(100),
	Role VARCHAR(20),
	Salary float,
	TotalExperience integer,
	password VARCHAR(100),
	createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	active INTEGER DEFAULT 0,
);

create table Membersubscription (
	id integer PRIMARY KEY auto_increment, 
    Userid INTEGER FOREIGN KEY REFERENCES user(id);
	Subscriptionstartdate  date,
	Subscriptionenddate date,
    Subscriptionamount FLOAT
	createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);